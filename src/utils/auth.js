import Cookies from 'js-cookie'

const TokenKey = 'Authorization'

export function getToken() {
  return  sessionStorage.getItem(TokenKey)
  // Cookies.get(TokenKey)
}

export function setToken(token) {
  return  sessionStorage.setItem(TokenKey, token);
  //  Cookies.set(TokenKey, token)
}

export function removeToken() {
  return sessionStorage.removeItem(TokenKey)
  // return Cookies.remove(TokenKey)
}
