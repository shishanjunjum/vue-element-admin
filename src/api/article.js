import request from '@/utils/request'
import { date } from 'jszip/lib/defaults'

export function fetchList(query) {
  return request({
    url: '/vue-element-admin/article/list',
    method: 'get',
    params: query
  })
}


/**
 * 
 * 管理员列表
 */
export function managerList(query){
  return request({
    url:'/back/manager/getManagerList',
    method:'post',
    params: {"userAccount":query.userAccount,"userName":query.userName,"roleId":query.roleId,"page":query.page,"limit":query.limit}
  })

}

export function userList(query){
  return request({
    url:'/back/user/userPage',
    method:'post',
    params:{"nickName":query.nickName,"phone":query.phone,"openId":query.openId,"page":query.page,"limit":query.limit}
  })
}

export function getRoleList(data){
  return request({
    url:'/back/role/getRoleList',
    method: 'get',
    params: {"roleType":data}
  } )
}

export function saveManager(data){
   return request({
     url: '/back/manager/saveManager',
     method: 'post',
     params: {"userAccount":data.userAccount,"userName":data.userName,"roleId":data.roleId,"password":data.password,"state":data.state}

   })
}


export function delManager(data){
  return request({
    url:'back/manager/delManager',
    method: 'post',
    params: {"mid":data.managerId}

  })

}

export function updateState(data,state){
  return request({
    url:'back/manager/updateState',
    method: 'post',
    params: {"managerId":data.managerId,"state":state}
  })

}


export function userupdateState(data,state){
  return request({
    url :"back/user/userupdateState",
    method: 'post',
    params: {"id":data.userId,"state":state}
  })
}

export function userRoleAdd(data,uid,state){
  return request({
    url:"back/user/userRoleAdd",
    method:'post',
    params:{"roleId":data.roleId,"uid":uid,"state":state}
  })
}

export function userDel(data){
  return request({
    url:"back/user/userDel",
    method:'post',
    params:{"id":data.userId}
  })
}


export function userSave(data){
  return request({
    url:"back/user/userSave",
    method:'post',
    params:{"nickName":data.nickName,"password":data.password,"phone":data.openId,"roleId":data.roleId}
  }) 
}

export function getRoleListByUid(data){
  return request({
    url:"back/user/getRolesByUid",
    method:'post',
    params:{"uid":data.userId}

  })
}

export function fetchArticle(id) {
  return request({
    url: '/vue-element-admin/article/detail',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/vue-element-admin/article/pv',
    method: 'get',
    params: { pv }
  })
}


export function createArticle(data) {
  return request({
    url: '/vue-element-admin/article/create',
    method: 'post',
    data
  })
}

export function updateArticle(data) {
  return request({
    url: '/vue-element-admin/article/update',
    method: 'post',
    data
  })
}
