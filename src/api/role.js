import request from '@/utils/request'

export function getRoutes(data) {
  return request({
    url: '/back/permission/getPsermissionByType',
    method: 'post',
    params:{"type":data}
  })
}

export function getRoles() {
  return request({
    url: '/vue-element-admin/roles',
    method: 'get'
  })
}



export function addRole(name,desc,data) {
  return request({
    url: `/back/role/addRole/`,
    method: 'post',
    params:{"name":name,"desc":desc,"data":data}
  })
}


export function getRoleList(data){
  return request({
    url:'/back/role/getRoleList',
    method: 'get',
    params: {"roleType":data}
  } )
}

export function getPermissionList(type){
  return request({
    url:'/back/permission/getPermissionList',
    method:'get',
    params:{"type":type}
  })

}

export function updatePermission(id,path,name,parent,desc){
  return request({
    url:`/back/permission/updatePermission/${id}`,
    method:'post',
    params:{"parent":parent,"name":name,"desc":desc,"path":path}
  })
}

export function deletePermission(id){
  return request({
    url:`/back/permission/deletePermission/${id}`,
    method:'get',
  })
}

export function addPermission(path,name,parent,desc,type){
  return request({
    url:`/back/permission/addPermission`,
    method: 'post',
    params:{"parent":parent,"name":name,"desc":desc,"path":path,"type":type}
  })
}

export function updateRole(id, name,desc,data) {
  return request({
    url: `/back/role/updateRole/${id}`,
    method: 'post',
    params:{"data":data,"name":name,"desc":desc}
  })
}

export function deleteRole(roleId) {
  return request({
    url: `/back/role/delRole/${roleId}`,
    method: 'get'
  })
}
