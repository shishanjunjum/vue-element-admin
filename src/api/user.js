import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/back/manager/login',
    method: 'post',
    params: data
  })
}

export function getInfo(token) {
  return request({
    url: '/back/manager/info',
    method: 'post',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/vue-element-admin/user/logout',
    method: 'post'
  })
}
